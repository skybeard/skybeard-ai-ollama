import json
import logging
import time
import datetime
import uuid
import requests
import telegram
import dataset
import re
import sys
import json_stream
from telegram import Update, MessageEntity, Bot
from telegram.ext import ApplicationBuilder, CommandHandler, ContextTypes
from config import config_from_yaml
from voice import VoiceGen

cfg = config_from_yaml("./skybeard-telegram-config.yaml", read_from_file=True)


#MODELS = cfg.models.as_dict()
CONTEXTS = {}

#configure logging
logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
    level=logging.INFO
)
logging.getLogger("httpx").setLevel(logging.WARNING)

def get_command(message):
    """extract the command that was used to call the handler"""
    cmds = message.parse_entities(types=[MessageEntity.BOT_COMMAND])
    return str(cmds[list(cmds.keys())[0]]).strip('/').split("@")[0]

def reply_check(s):
    stripped_s = re.sub('\s+', ' ', s).strip()
    if s:
        return True
    return False

def update_model_context(chat_id, model, context):
    """
    update the stored context embeddings for a specific chat and model
    """
    if chat_id not in CONTEXTS:
        CONTEXTS[chat_id] = {model: context}
    else:
        CONTEXTS[chat_id][model] = context


#def get_model_context(chat_id, model):
#    """
#    get the stored context embeddings for a specific chat and model
#    """
#    context = None
#    try:
#        context = CONTEXTS[chat_id][model]
#    except KeyError:
#        pass
#    return context

def get_model_context(message):
    try:
        with dataset.connect(cfg.database.uri) as tx:
            record = tx['context'].find_one(
                message_id = message.id,
                chat_id = message.chat_id)
            context = json.loads(record['context'])
            model = record['model']
            logging.info('found context for {}: {}'.format(model, context))
            return context, model
    except Exception as e:
        logging.error("Unable to retrieve context", e)
        return None, None

def store_model_context(message, model, context):
    try:
        context = json.dumps(context)
        with dataset.connect(cfg.database.uri) as tx:
            tx['context'].insert(
                dict(
                chat_id=message.chat_id,
                message_id=message.id,
                model=model,
                context=context))
    except Exception as e:
        logging.error("Unable to save context", e)


def ollama_generate(text, chat_id, model, template=None, model_context=None):
    """
    invoke ollama API
    """
    url = cfg.ollama_server

    data = dict(
        model = model,
        prompt = text
    )

    if model_context:
        data['context'] = model_context
    if template:
        data['template'] = template
    logging.info(data)
    payload = json.dumps(data)
    headers = {'Content-Type': 'application/json'}

    # from https://github.com/jmorganca/ollama/blob/main/api/client.py
    for i in range(0,3):
        if i > 0:
            print("here")
            logging.info("Generation attempt failed. Trying again (attempt ({}/3)".format(i+1))
        with requests.post(url, headers=headers, data=payload, stream=True) as response:
                response.raise_for_status()
                final_context = None
                full_response = ""
                for line in response.iter_lines():
                    if line:
                        chunk = json.loads(line)
                        if not chunk.get("done"):
                            response_piece = chunk.get("response", "")
                            full_response += response_piece
                            print(response_piece, end="", flush=True)
                    
                        if chunk.get("done"):
                            final_context = chunk.get("context")
        if reply_check(full_response):
            break
        print(repr(full_response)) 

    return full_response, final_context

async def model_command_handler(update: Update, context: ContextTypes.DEFAULT_TYPE):
    """
    handle replies from telegram
    """
    await context.bot.send_chat_action(
        chat_id=update.effective_message.chat_id,
        action=telegram.constants.ChatAction.TYPING)
    quote = update.message.chat.type != telegram.constants.ChatType.PRIVATE
    
    cmd = get_command(update.message)
    chat_id = update.effective_message.chat_id
    model = None
    template= None

    models = cfg.models

    reply = 'Something went wrong'
    #handle missing model in config
    try:
        model = models[cmd].as_dict()
        
    except KeyError as e:
        logging.error(e)
        await update.message.reply_text("That artificial consciousness does not appear to be loaded into my nexus")

    model_name = model['name']
    try:
        template = model['template']
        logging.info("loading dynamic template for model {}".format(model_name))
        user = update.message.from_user.first_name
        template = template.replace('--user--', user)
    except KeyError:
        logging.info("No custom template found, using Modelfile default")
    except Exception:
        logging.error(e) 
        await update.message.reply_text(reply)
    
    input_text = ' '.join(context.args)
    new_model_context = None
    try:
        reply, new_model_context = ollama_generate(input_text, chat_id, model_name, template)
    except Exception as e:
        logging.error(e)
        await update.message.reply_text("something went wrong during generation", quote=quote)
        return
    if not reply_check(reply):
        reply = '...'
    try:
        sent_message = await update.message.reply_text(reply, quote=quote)
    except Exception as e:
        logging.error(e)
        await update.message.reply_text("something went wrong, please try again", quote=quote)
        return
    store_model_context(sent_message, cmd, new_model_context)

async def model_reply_handler(update: Update, context: ContextTypes.DEFAULT_TYPE):
    """
    handle commands from telegram
    """
    await context.bot.send_chat_action(
        chat_id=update.effective_message.chat_id,
        action=telegram.constants.ChatAction.TYPING)
    quote = update.message.chat.type != telegram.constants.ChatType.PRIVATE

    quoted_message = update.message.reply_to_message
    if not quoted_message:
        await update.message.reply_text("What conversation should I continue?")
        return
    me = await context.bot.get_me()
    
    if quoted_message.from_user.id != context.bot.id:
        await update.message.reply_text("Did you quote the wrong message?")
        return

    
    chat_id = update.effective_message.chat_id
    model = None
    template= None

    models = cfg.models

    reply = 'Something went wrong'
    #handle missing model in config
    try:
        model_context, cmd = get_model_context(quoted_message)
        model = models[cmd].as_dict()
        
    except KeyError as e:
        logging.error(e)
        await update.message.reply_text("That artificial consciousness does not appear to be loaded into my nexus")

    model_name = model['name']
    try:
        template = model['template']
        logging.info("loading dynamic template for model {}".format(model_name))
        user = update.message.from_user.first_name
        template = template.replace('--user--', user)
    except KeyError:
        logging.info("No custom template found, using Modelfile default")
    except Exception:
        logging.error(e) 
        await update.message.reply_text(reply)
    new_model_context = None
    input_text = ' '.join(context.args)
    try:
        reply, new_model_context = ollama_generate(input_text, chat_id, model_name, template, model_context = model_context)
    except Exception as e:
        logging.error(e)
        await update.message.reply_text("something went wrong during generation", quote=quote)
        return
    if not reply_check(reply):
        reply = '...'
    if not model_context:
        reply = "I have no recollection of this conversation, however...\n{}".format(reply)
    sent_message = await update.message.reply_text(reply, quote=quote)
    store_model_context(sent_message, cmd, new_model_context)



async def voice_command_handler(update: Update, context: ContextTypes.DEFAULT_TYPE):
    """
    handle voice replies from telegram
    """
    await context.bot.send_chat_action(
        chat_id=update.effective_message.chat_id,
        action=telegram.constants.ChatAction.RECORD_VOICE)
    quote = update.message.chat.type != telegram.constants.ChatType.PRIVATE
    
    #TODO: move to config
    model_name = cfg.models.darkbeard.name
    template= cfg.models.darkbeard.template
    user = update.message.from_user.first_name
    template = template.replace('--user--', user)


    reply = 'Something went wrong'
    #handle missing model in config

    input_text = ' '.join(context.args)
    
    try:
        chat_id=update.effective_message.chat_id
        reply, new_model_context = ollama_generate(input_text, chat_id, model_name, template)
    except Exception as e:
        logging.error(e)
        await update.message.reply_text("something went wrong during generation", quote=quote)
        return
    if not reply_check(reply):
        reply = '...'

    voice_generator = VoiceGen(reply, update.effective_message.chat_id)
    voice_fname = voice_generator.generate_ogg()

    try:
        voice_file = open(voice_fname, 'rb')
        if voice_fname.split('.')[-1] == 'ogg':
            await context.bot.send_voice( voice=voice_file, chat_id=update.effective_message.chat_id)
        else:
            await context.bot.send_document( document=voice_file, chat_id=update.effective_message.chat_id)
    except Exception as e:
        logging.error(e)
        await update.message.reply_text("something went wrong, please try again", quote=quote)
        return


async def skb_voice_command_handler(update: Update, context: ContextTypes.DEFAULT_TYPE):
    """
    handle voice replies from telegram
    """
    await context.bot.send_chat_action(
        chat_id=update.effective_message.chat_id,
        action=telegram.constants.ChatAction.RECORD_VOICE)
    quote = update.message.chat.type != telegram.constants.ChatType.PRIVATE
    
    #TODO: move to config
    model_name = cfg.models.skybeard.name
    user = update.message.from_user.first_name


    reply = 'Something went wrong'
    #handle missing model in config

    input_text = ' '.join(context.args)
    
    try:
        chat_id=update.effective_message.chat_id
        reply, new_model_context = ollama_generate(input_text, chat_id, model_name)
    except Exception as e:
        logging.error(e)
        await update.message.reply_text("something went wrong during generation", quote=quote)
        return
    if not reply_check(reply):
        reply = '...'

    voice_generator = VoiceGen(reply, update.effective_message.chat_id)
    voice_fname = voice_generator.generate_ogg()

    try:
        voice_file = open(voice_fname, 'rb')
        if voice_fname.split('.')[-1] == 'ogg':
            await context.bot.send_voice( voice=voice_file, chat_id=update.effective_message.chat_id)
        else:
            await context.bot.send_document( document=voice_file, chat_id=update.effective_message.chat_id)
    except Exception as e:
        logging.error(e)
        await update.message.reply_text("something went wrong, please try again", quote=quote)
        return

async def tts_command_handler(update: Update, context: ContextTypes.DEFAULT_TYPE):
    await context.bot.send_chat_action(
        chat_id=update.effective_message.chat_id,
        action=telegram.constants.ChatAction.UPLOAD_DOCUMENT)
    quote = update.message.chat.type != telegram.constants.ChatType.PRIVATE

    input_text = ' '.join(context.args)

    voice_generator = VoiceGen(input_text, update.effective_message.chat_id)
    voice_fname = voice_generator.generate_ogg()
    try:
        voice_file = open(voice_fname, 'rb')
        await context.bot.send_document( document=voice_file, chat_id=update.effective_message.chat_id)
    except Exception as e:
        logging.error(e)
        await update.message.reply_text("something went wrong, please try again", quote=quote)
        return




if __name__ == '__main__':
    
    telegram_config = cfg.telegram.as_dict()
    application = ApplicationBuilder().token(telegram_config['api_token']).build()

    commands = cfg.models.keys()
    logging.info("Regsitering the following commands for model handler: {}".format(', '.join(commands)))
    
    command_handler = CommandHandler(commands, model_command_handler)
    application.add_handler(command_handler)

    reply_handler = CommandHandler('continue', model_reply_handler)
    application.add_handler(reply_handler)


    voice_handler = CommandHandler('voicebeard',voice_command_handler)
    application.add_handler(voice_handler)
    
    skb_voice_handler = CommandHandler('skyvoice',skb_voice_command_handler)
    application.add_handler(skb_voice_handler)
    
    tts_handler = CommandHandler('tts', tts_command_handler)
    application.add_handler(tts_handler)
    
    application.run_polling()
